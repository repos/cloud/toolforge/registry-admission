apiVersion: apps/v1
kind: Deployment
metadata:
  name: registry-admission
  labels:
    name: registry-admission
  annotations:
    secret.reloader.stakater.com/reload: registry-admission-certs
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app.kubernetes.io/instance: registry-admission
      app.kubernetes.io/name: registry-admission
  template:
    metadata:
      name: registry-admission
      labels:
        app.kubernetes.io/instance: registry-admission
        app.kubernetes.io/name: registry-admission
    spec:
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels:
              app.kubernetes.io/instance: registry-admission
              app.kubernetes.io/name: registry-admission
      containers:
        - name: webhook
          image: "{{ .Values.image.name }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          resources: {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - name: webhook-certs
              mountPath: /etc/webhook/certs
              readOnly: true
          securityContext:
            readOnlyRootFilesystem: true
          env:
            - name: REGISTRIES
              value: "{{ .Values.allowedRegistries | join "," }}"
            - name: EXCLUDEDNAMESPACES
              value: "{{ .Values.excludedNamespaces | join "," }}"
      volumes:
        - name: webhook-certs
          secret:
            secretName: registry-admission-certs
